#!/usr/bin/python

def main(debug):
   print('please don\'t delete me!')

if __name__ == "__main__":

   import argparse
   parser = argparse.ArgumentParser()
   parser.add_argument("-v", "--debug", help="Turn on debug messages", action="store_true", default=False)
   args = parser.parse_args()
   debug = args.debug

   main(debug)

